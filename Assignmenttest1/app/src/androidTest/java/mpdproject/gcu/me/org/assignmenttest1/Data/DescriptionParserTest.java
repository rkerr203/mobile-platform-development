package mpdproject.gcu.me.org.assignmenttest1.Data;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ryank on 27/03/2018.
 */
public class DescriptionParserTest {



    @Test
    public void testParseIncidentsDelayInformation() throws Exception {

        TrafficScotlandData incidentsDataObject = new CurrentIncidents();
        String incidentsDescription = "4 lanes restricted Westbound indefinitely";

        DescriptionParser incidentsParser = new DescriptionParser(incidentsDescription, incidentsDataObject);

        incidentsParser.parseDelayInformation();

        assertNotNull(incidentsDataObject.getShortDescription());
        assertEquals(incidentsDescription, incidentsDataObject.getShortDescription());
    }

    @Test
    public void testParseRoadworksDelayInformation() throws Exception {

        TrafficScotlandData roadworksDataObject = new Roadworks();
        String roadworksDescription = "Start Date: Monday, 25 September 2017 - 00:00<br />End Date: Thursday, 31 May 2018 - 00:00<br />Delay Information: No reported delay.";
        String expected = "No reported delay.";
        DescriptionParser roadworksParser = new DescriptionParser(roadworksDescription, roadworksDataObject);
        roadworksParser.parseDelayInformation();

        assertNotNull(roadworksDataObject.getShortDescription());
        assertEquals(expected, roadworksDataObject.getShortDescription());
    }

    @Test
    public void testParseDelayInformationWithNoInformation() throws Exception {

        TrafficScotlandData data = new TrafficScotlandData();
        String description = "Test data";
        String expected = "N/A";
        DescriptionParser parser = new DescriptionParser(description, data);
        parser.parseDelayInformation();

        assertNotNull(data.getShortDescription());
        assertEquals(expected, data.getShortDescription());
    }


    @Test
    public void testParseDate() throws Exception {

        TrafficScotlandData data = new TrafficScotlandData();
        String description = "Start Date: Friday, 30 March 2018 - 00:00<br />End Date: Monday, 30 April 2018 - 00:00<br />Works: Third Party Works Traffic Management: Contraflow (50mph) Diversion Information: Temporary Diversion of Traffic over Slip Roads for roundabout at Blackdog.";
        String expectedStart = "30 March 2018";
        String expectedEnd = "30 April 2018";
        DescriptionParser parser = new DescriptionParser(description, data);
        parser.parseDates();

        assertNotNull(data.getStartDate());
        assertEquals(expectedStart, data.getStartDate());

        assertNotNull(data.getEndDate());
        assertEquals(expectedEnd, data.getEndDate());

        assertNotNull(data.getDuration());
        Long expected = Long.valueOf(31);
        Long actual = data.getDuration();
        assertEquals(expected, actual);

        DescriptionParser parserInvalidData = new DescriptionParser(description, data);
        parser.setDescription("test");
        parser.parseDates();

        assertEquals("N/A", data.getStartDate());
    }

    @Test
    public void testParseWorksInformation() throws Exception {

        TrafficScotlandData data = new TrafficScotlandData();
        String description = "Start Date: Friday, 30 March 2018 - 00:00<br />End Date: Monday, 30 April 2018 - 00:00<br />Works: Third Party Works Traffic Management: Contraflow (50mph) Diversion Information: Temporary Diversion of Traffic over Slip Roads for roundabout at Blackdog.";

        DescriptionParser parser = new DescriptionParser(description, data);
        parser.parseWorksInformation();

        String expected = "Third Party Works";
        assertNotNull(data.getWorksInformation());
        assertEquals(expected, data.getWorksInformation().trim());

        parser.setDescription("test");
        parser.parseWorksInformation();

        assertEquals("N/A", data.getWorksInformation().trim());
    }

    @Test
    public void testParseTrafficManagementInformation() throws Exception {

        TrafficScotlandData data = new TrafficScotlandData();
        String description = "Start Date: Thursday, 29 March 2018 - 00:00<br />End Date: Sunday, 01 April 2018 - " +
                                "00:00<br />Works: Resurfacing Traffic Management: Road Closure. Diversion Information: " +
                                    "During this period traffic traveling to Edinburgh from the M90 will be diverted via the M90 Spur and the A8 Glasgow Road. " +
                                        "Traffic from the Forth Road Bridge will be diverted via the B907 and B924.";

        DescriptionParser parser = new DescriptionParser(description, data);
        parser.parseTrafficManagementInformation();

        String expected = "Road Closure.";
        assertNotNull(data.getTrafficManagementInformation());
        assertEquals(expected, data.getTrafficManagementInformation().trim());

        parser.setDescription("Start Date: Thursday, 29 March 2018 - 00:00<br />End Date: Sunday, 01 April 2018 - " +
                                     "00:00<br />Works: Resurfacing Traffic Management: Road Closure.");
        parser.parseTrafficManagementInformation();

        assertEquals(expected, data.getTrafficManagementInformation().trim());

        parser.setDescription("test");
        parser.parseTrafficManagementInformation();
        assertEquals("N/A", data.getTrafficManagementInformation());

    }


    @Test
    public void testParseDiversionInformation() throws Exception {

        TrafficScotlandData data = new TrafficScotlandData();
        String description = "Start Date: Thursday, 29 March 2018 - 00:00<br />End Date: Sunday, 01 April 2018 - " +
                "00:00<br />Works: Resurfacing Traffic Management: Road Closure. Diversion Information: " +
                "During this period traffic traveling to Edinburgh from the M90 will be diverted via the M90 Spur and the A8 Glasgow Road. " +
                "Traffic from the Forth Road Bridge will be diverted via the B907 and B924.";

        DescriptionParser parser = new DescriptionParser(description, data);
        parser.parseDiversionInformation();

        String expected = "During this period traffic traveling to Edinburgh from the M90 will be diverted via the M90 Spur and the A8 Glasgow Road. " +
                                "Traffic from the Forth Road Bridge will be diverted via the B907 and B924.";

        assertNotNull(data.getDiversionDetails());
        assertEquals(expected, data.getDiversionDetails().trim());


        parser.setDescription("Start Date: Thursday, 29 March 2018 - 00:00<br />End Date: Sunday, 01 April 2018 - " +
                                    "00:00<br />Works: Resurfacing Traffic Management: Road Closure.");
        parser.parseDiversionInformation();
        assertEquals("N/A", data.getDiversionDetails());

    }



}