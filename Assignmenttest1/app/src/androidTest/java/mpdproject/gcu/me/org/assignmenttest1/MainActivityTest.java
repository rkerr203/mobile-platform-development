package mpdproject.gcu.me.org.assignmenttest1;

import android.app.Instrumentation;
import android.support.test.rule.ActivityTestRule;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by root on 21/03/18.
 * Ryan Kerr S1433346
 */
public class MainActivityTest{

    @Rule
    public ActivityTestRule<MainActivity> mainActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    private MainActivity mainTest = null;

    Instrumentation.ActivityMonitor monitor = getInstrumentation().addMonitor(TrafficInformationActivity.class.getName(), null, false);

    @Before
    public void setUp() throws Exception {
        mainTest = mainActivityTestRule.getActivity();
    }

    @Test
    public void testCheckToolBarDisplayed(){

        View toolbar = mainTest.findViewById(R.id.my_toolbar);
        assertNotNull(toolbar);

    }

    @Test
    public void testCheckButtonsDisplayed(){

        Button roadworksButton = (Button) mainTest.findViewById(R.id.roadworksButton);
        Button plannedRoadworksButton = (Button) mainTest.findViewById(R.id.plannedRoadworksButton);
        Button incidentsButton = (Button) mainTest.findViewById(R.id.incidentsButton);

        assertNotNull(roadworksButton);
        assertNotNull(plannedRoadworksButton);
        assertNotNull(incidentsButton);

    }

    @Test
    public void testLoadingIconHidden(){

        View spinner = mainTest.findViewById(R.id.loadingPanel);

        assertEquals(8, spinner.getVisibility());
    }

    @Test
    public void testMainText(){

        final String expected = "Welcome to Traffic Scotland";
        TextView text = (TextView) mainTest.findViewById(R.id.urlInput);

        assertEquals(expected, text.getText());
    }

    @Test
    public void testButton(){

        onView(withId(R.id.incidentsButton)).perform(click());

        TrafficInformationActivity tActivity = (TrafficInformationActivity) getInstrumentation().waitForMonitorWithTimeout(monitor, 5000);

        assertNotNull(tActivity);

        View listView = tActivity.findViewById(R.id.simpleListView);

        assertNotNull(listView);

        tActivity.finish();

    }


    @After
    public void tearDown() throws Exception {
    }

}