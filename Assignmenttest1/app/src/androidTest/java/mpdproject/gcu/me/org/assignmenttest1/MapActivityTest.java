package mpdproject.gcu.me.org.assignmenttest1;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.test.rule.ActivityTestRule;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TextView;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;

import java.util.ArrayList;

import mpdproject.gcu.me.org.assignmenttest1.Data.TrafficScotlandData;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;



/**
 * Created by root on 21/03/18.
 * Ryan Kerr S1433346
 */
public class MapActivityTest {

    @Rule
    public ActivityTestRule<TrafficInformationActivity> trafficInformationActivityActivityTestRule = new ActivityTestRule<TrafficInformationActivity>(TrafficInformationActivity.class, true, false);

    @Rule
    public ActivityTestRule<MapsActivity> mapsActivityTestRule = new ActivityTestRule<>(MapsActivity.class, true, false);


    private MapsActivity mapsTest = null;

    private TrafficInformationActivity trafficTestData = null;

    @Before
    public void setUp() throws Exception {

        Intent trafficIntent = new Intent();
        trafficIntent.putExtra("DATA", "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>\n" +
                "<rss version=\"2.0\" xmlns:georss=\"http://www.georss.org/georss\" xmlns:gml=\"http://www.opengis.net/gml\">\n" +
                "  <channel>\n" +
                "    <title>Traffic Scotland - Current Incidents</title>\n" +
                "    <description>Current incidents on the road network e.g. accidents</description>\n" +
                "    <link>https://trafficscotland.org/currentincidents/</link>\n" +
                "    <language />\n" +
                "    <copyright />\n" +
                "    <managingEditor />\n" +
                "    <webMaster />\n" +
                "    <lastBuildDate>Sat, 24 Mar 2018 21:05:05 GMT</lastBuildDate>\n" +
                "    <docs>https://trafficscotland.org/rss/</docs>\n" +
                "    <rating />\n" +
                "    <generator>Traffic Scotland | www.trafficscotland.org</generator>\n" +
                "    <ttl>5</ttl>\n" +
                "    <item>\n" +
                "      <title>A77 A714 Girvan(S)- B7044 Ballantrae - Closure</title>\n" +
                "      <description>The A77 is closed in both directions between Shallochpark Roundabout Girvan and the Cairnryan Ferry Terminal for weekend roadworks.</description>\n" +
                "      <link>http://tscot.org/01c195356</link>\n" +
                "      <georss:point>55.174916533661 -4.93019329549961</georss:point>\n" +
                "      <author />\n" +
                "      <comments />\n" +
                "      <pubDate>Fri, 23 Mar 2018 20:51:32 GMT</pubDate>\n" +
                "    </item>\n" +
                "    <item>\n" +
                "      <title>M8 J12 Between Slips - Closure</title>\n" +
                "      <description>The M8 is closed Eastbound at Junction 12 (Cumbernauld Road) for overnight roadworks.</description>\n" +
                "      <link>http://tscot.org/01c195394</link>\n" +
                "      <georss:point>55.8741263193988 -4.18646064331918</georss:point>\n" +
                "      <author />\n" +
                "      <comments />\n" +
                "      <pubDate>Sat, 24 Mar 2018 20:33:53 GMT</pubDate>\n" +
                "    </item>\n" +
                "    <item>\n" +
                "      <title>M8 J29 - A737 junction - Closure</title>\n" +
                "      <description>The M8 is closed Westbound at Junction 29 (St James) for overnight roadworks</description>\n" +
                "      <link>http://tscot.org/01c195395</link>\n" +
                "      <georss:point>55.8587459696208 -4.4453520002335</georss:point>\n" +
                "      <author />\n" +
                "      <comments />\n" +
                "      <pubDate>Sat, 24 Mar 2018 21:05:05 GMT</pubDate>\n" +
                "    </item>\n" +
                "  </channel>\n" +
                "</rss>");
        trafficIntent.putExtra("TYPE", "INCIDENT");

        trafficTestData = trafficInformationActivityActivityTestRule.launchActivity(trafficIntent);
        Intent intent = new Intent();
        intent.putExtra("clickedItem", "A77 A714 Girvan(S)- B7044 Ballantrae - Closure");
//        ArrayList<TrafficScotlandData> data = new ArrayList<>();
//        data.add(new TrafficScotlandData("A77 A714 Girvan(S)- B7044 Ballantrae - Closure", "test","test","test","test","test"));
        mapsTest = mapsActivityTestRule.launchActivity(intent);
//        mapsTest.parseMapInformation(data, "A77 A714 Girvan(S)- B7044 Ballantrae - Closure");
    }

    @Test
    public void testCheckToolBarDisplayed() {

        View toolbar = mapsTest.findViewById(R.id.my_toolbar);
        assertNotNull(toolbar);

    }

    @Test
    public void testCheckMapDisplayed() {

        View map = mapsTest.findViewById(R.id.map);
        assertNotNull(map);

    }

    @Test
    public void testMapTitleDisplayed() {

        TextView mapTitle = (TextView) mapsTest.findViewById(R.id.mapTitle);
        assertNotNull(mapTitle);
    }

    @Test
    public void testMapDetails() {

        TextView startDate = (TextView) mapsTest.findViewById(R.id.startDate);
        TextView endDate = (TextView) mapsTest.findViewById(R.id.endDate);
        TextView workDetails = (TextView) mapsTest.findViewById(R.id.workDetails);
        TextView trafficManagementDetails = (TextView) mapsTest.findViewById(R.id.trafficManagementDetails);
        TextView diversionDetails = (TextView) mapsTest.findViewById(R.id.diversionDetails);
        TextView descriptionDetails = (TextView) mapsTest.findViewById(R.id.descriptionDetails);

        assertNotNull(startDate);
        assertEquals("N/A", startDate.getText());
        assertNotNull(endDate);
        assertEquals("N/A", startDate.getText());
        assertNotNull(workDetails);
        assertEquals("N/A", startDate.getText());
        assertNotNull(descriptionDetails);
        assertEquals("The A77 is closed in both directions between Shallochpark Roundabout Girvan and the Cairnryan Ferry Terminal for weekend roadworks.", descriptionDetails.getText());
        assertNotNull(trafficManagementDetails);
        assertNotNull(diversionDetails);


    }

    @Ignore
    @Test
    public void testFlipper() {

        mapsTest.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        TextView infoText = (TextView) mapsTest.findViewById(R.id.moreInfo);

        assertNotNull(infoText);
        assertEquals("Change Display", infoText.getText());

        onView(withId(R.id.moreInfo)).perform(click());

        TableLayout table = (TableLayout) mapsTest.findViewById(R.id.landscape_table);

        assertNotNull(table);

    }


    @After
    public void tearDown() throws Exception {
    }

}