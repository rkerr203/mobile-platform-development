package mpdproject.gcu.me.org.assignmenttest1;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Created by ryank on 24/03/2018.
 * Ryan Kerr S1433346
 */

@RunWith(Suite.class)
@SuiteClasses({
        MainActivityTest.class,
        TrafficInformationActivityTest.class,
        MapActivityTest.class})

public class TestSuite {
}
