package mpdproject.gcu.me.org.assignmenttest1;

import android.app.Instrumentation;
import android.content.Intent;
import android.support.test.espresso.DataInteraction;
import android.support.test.rule.ActivityTestRule;
import android.view.View;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static junit.framework.Assert.assertNull;
import static org.hamcrest.CoreMatchers.anything;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by ryank on 24/03/2018.
 * Ryan Kerr S1433346
 */
public class TrafficInformationActivityTest {

    @Rule
    public ActivityTestRule<TrafficInformationActivity> trafficInformationActivityActivityTestRule = new ActivityTestRule<TrafficInformationActivity>(TrafficInformationActivity.class, true, false);

    private TrafficInformationActivity trafficTest = null;

    Instrumentation.ActivityMonitor monitor = getInstrumentation().addMonitor(MapsActivity.class.getName(), null, false);

    @Before
    public void setUp() throws Exception {
            Intent intent = new Intent();
            intent.putExtra("DATA", "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>\n" +
                            "<rss version=\"2.0\" xmlns:georss=\"http://www.georss.org/georss\" xmlns:gml=\"http://www.opengis.net/gml\">\n" +
                            "  <channel>\n" +
                            "    <title>Traffic Scotland - Current Incidents</title>\n" +
                            "    <description>Current incidents on the road network e.g. accidents</description>\n" +
                            "    <link>https://trafficscotland.org/currentincidents/</link>\n" +
                            "    <language />\n" +
                            "    <copyright />\n" +
                            "    <managingEditor />\n" +
                            "    <webMaster />\n" +
                            "    <lastBuildDate>Sat, 24 Mar 2018 21:05:05 GMT</lastBuildDate>\n" +
                            "    <docs>https://trafficscotland.org/rss/</docs>\n" +
                            "    <rating />\n" +
                            "    <generator>Traffic Scotland | www.trafficscotland.org</generator>\n" +
                            "    <ttl>5</ttl>\n" +
                            "    <item>\n" +
                            "      <title>A77 A714 Girvan(S)- B7044 Ballantrae - Closure</title>\n" +
                            "      <description>The A77 is closed in both directions between Shallochpark Roundabout Girvan and the Cairnryan Ferry Terminal for weekend roadworks.</description>\n" +
                            "      <link>http://tscot.org/01c195356</link>\n" +
                            "      <georss:point>55.174916533661 -4.93019329549961</georss:point>\n" +
                            "      <author />\n" +
                            "      <comments />\n" +
                            "      <pubDate>Fri, 23 Mar 2018 20:51:32 GMT</pubDate>\n" +
                            "    </item>\n" +
                            "    <item>\n" +
                            "      <title>M8 J12 Between Slips - Closure</title>\n" +
                            "      <description>The M8 is closed Eastbound at Junction 12 (Cumbernauld Road) for overnight roadworks.</description>\n" +
                            "      <link>http://tscot.org/01c195394</link>\n" +
                            "      <georss:point>55.8741263193988 -4.18646064331918</georss:point>\n" +
                            "      <author />\n" +
                            "      <comments />\n" +
                            "      <pubDate>Sat, 24 Mar 2018 20:33:53 GMT</pubDate>\n" +
                            "    </item>\n" +
                            "    <item>\n" +
                            "      <title>M8 J29 - A737 junction - Closure</title>\n" +
                            "      <description>The M8 is closed Westbound at Junction 29 (St James) for overnight roadworks</description>\n" +
                            "      <link>http://tscot.org/01c195395</link>\n" +
                            "      <georss:point>55.8587459696208 -4.4453520002335</georss:point>\n" +
                            "      <author />\n" +
                            "      <comments />\n" +
                            "      <pubDate>Sat, 24 Mar 2018 21:05:05 GMT</pubDate>\n" +
                            "    </item>\n" +
                            "  </channel>\n" +
                            "</rss>");
            intent.putExtra("TYPE", "INCIDENT");

            trafficTest = trafficInformationActivityActivityTestRule.launchActivity(intent);
        }

    @Test
    public void testIntent(){

        String data = trafficTest.getIntent().getStringExtra("DATA");
        String type = trafficTest.getIntent().getStringExtra("TYPE");
        assertNotNull(data);
        assertEquals("INCIDENT", type);
    }

    @Test
    public void testCheckToolBarDisplayed(){

        View toolbar = trafficTest.findViewById(R.id.search_toolbar);
        assertNotNull(toolbar);
        assertEquals("Incidents", trafficTest.getSupportActionBar().getTitle());
    }

    @Test
    public void testCheckToolBarOptions(){

        Boolean menuOpen = trafficTest.getSupportActionBar().openOptionsMenu();
        assertTrue(menuOpen);

    }

    @Test
    public void testListView(){

        assertNotNull(trafficTest.getListAdapter());
        int count = trafficTest.getListAdapter().getCount();
        assertNotEquals(0, count);
        assertNotEquals(2, count);

    }

    @Test
    public void testLoadingIconHidden(){

        View spinner = trafficTest.findViewById(R.id.trafficLoadingPanel);

        assertEquals(8, spinner.getVisibility());
    }

    @Test
    public void testListItemClick(){
        onData(anything()).inAdapterView(withId(R.id.simpleListView)).atPosition(0).perform(click());

        MapsActivity mActivity = (MapsActivity) getInstrumentation().waitForMonitorWithTimeout(monitor, 5000);

        assertNotNull(mActivity);

        View map = mActivity.findViewById(R.id.map);

        assertNotNull(map);

        mActivity.finish();
    }

}