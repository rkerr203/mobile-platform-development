package mpdproject.gcu.me.org.assignmenttest1.Async;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import mpdproject.gcu.me.org.assignmenttest1.MainActivity;
import mpdproject.gcu.me.org.assignmenttest1.R;
import mpdproject.gcu.me.org.assignmenttest1.TrafficInformationActivity;

/**
 * Created by ryank on 09/03/2018.
 * Ryan Kerr S1433346
 */

public class URLTask extends AsyncTask<String, String, Void> {

    private String result;
    private String url;
    private String type;
    private MainActivity activity;
    private TrafficInformationActivity trafficActivity;
    public boolean isToolbarSelection;

    public URLTask(MainActivity activityMain) {
        this.activity = activityMain;
        isToolbarSelection = false;
    }

    public URLTask(TrafficInformationActivity trafficActivity) {
        this.trafficActivity = trafficActivity;
        isToolbarSelection = true;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        System.out.println("TOOLBAR OPTION " + isToolbarSelection);
        if (isToolbarSelection == false) {
            activity.findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
        } else {
            trafficActivity.findViewById(R.id.trafficLoadingPanel).setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected Void doInBackground(String... strings) {
        result = "";
        URL aurl;
        URLConnection yc;
        BufferedReader in = null;
        String inputLine = "";
        type = strings[1];

        Log.e("MyTag", "in run");

        try {
            Log.e("MyTag", "in try");
            aurl = new URL(strings[0]);
            yc = aurl.openConnection();
            in = new BufferedReader(new InputStreamReader(yc.getInputStream()));

            while ((inputLine = in.readLine()) != null) {
                result = result + inputLine;
            }

            in.close();
            System.out.print(result);
        } catch (IOException ae) {
            Log.e("MyTag", "ioexception");
        }
        return null;
    }

    protected void onPostExecute(Void result) {
        if (isToolbarSelection == false) {
            activity.findViewById(R.id.loadingPanel).setVisibility(View.GONE);
            activity.startTrafficInformationActivity(this.result, type);
        } else {
            trafficActivity.findViewById(R.id.trafficLoadingPanel).setVisibility(View.GONE);
            trafficActivity.displayData(this.result, type);
        }
    }

}
