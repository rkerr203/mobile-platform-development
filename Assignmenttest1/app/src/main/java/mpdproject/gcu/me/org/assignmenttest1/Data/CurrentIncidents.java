package mpdproject.gcu.me.org.assignmenttest1.Data;

/**
 * Created by ryank on 10/02/2018.
 * Ryan Kerr S1433346
 */

public class CurrentIncidents extends TrafficScotlandData{

    public CurrentIncidents(){
    }

    public String toString() {
        return "CurrentIncidents{" +
                "title='" + super.getTitle() + '\'' +
                ", description='" + super.getDescription() + '\'' +
                ", link='" + super.getLink() + '\'' +
                ", coordinates=" + super.getCoordinates() +
                ", author='" + super.getAuthor() + '\'' +
                ", comments='" + super.getComments() + '\'' +
                ", date='" + super.getDate() + '\'' +
                '}';
    }
}
