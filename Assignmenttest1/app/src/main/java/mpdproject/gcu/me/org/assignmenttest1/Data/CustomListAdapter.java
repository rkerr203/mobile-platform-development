package mpdproject.gcu.me.org.assignmenttest1.Data;


import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.logging.Logger;

import mpdproject.gcu.me.org.assignmenttest1.MapsActivity;
import mpdproject.gcu.me.org.assignmenttest1.R;
import mpdproject.gcu.me.org.assignmenttest1.TrafficInformationActivity;

/**
 * Created by ryank on 11/03/2018.
 * Ryan Kerr S1433346
 */

public class CustomListAdapter extends ArrayAdapter<String> implements AdapterView.OnItemClickListener{

    private final TrafficInformationActivity context;
    private final ArrayList<String> titles;
    private final ArrayList<Long> durations;
    private final Integer[] imageId;

    public CustomListAdapter(TrafficInformationActivity context,
                             ArrayList<String> titles, Integer[] imageId, ArrayList<Long> durations) {
        super(context, R.layout.simplerow, titles);
        this.context = context;
        this.titles = titles;
        this.imageId = imageId;
        this.durations = durations;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.simplerow, null, true);
        rowView.setMinimumHeight(120);
        TextView title = (TextView) rowView.findViewById(R.id.rowTextView);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.durationIcon);

        TextView testView = (TextView) rowView.findViewById(R.id.secondLineTest);

        String duration = titles.get(position);

        title.setText(titles.get(position));

        if(durations.get(position) == null){
            System.out.println("No durations so item is INCIDENTS");
            rowView.findViewById(R.id.secondLineTest).setVisibility(View.GONE);
        }
        else{
            testView.setText("Roadworks lasting " + durations.get(position).toString() + " days");
        }
        imageView.setImageResource(imageId[position]);
        return rowView;

    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            String clickedItem = adapterView.getItemAtPosition(i).toString();
            Intent intent = new Intent(context, MapsActivity.class);
            intent.putExtra("clickedItem", clickedItem);

            // Pass control to the map activity when data is clicked
            context.startActivity(intent);
    }
}
