package mpdproject.gcu.me.org.assignmenttest1.Data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ryank on 15/03/2018.
 * Ryan Kerr S1433346
 */

public class DescriptionParser {

    private String description;
    private TrafficScotlandData data;

    /**
     * A helper class for the Pull Parser, takes responsibility for extracting description information appropriately
     * @param description
     * @param data
     */
    public DescriptionParser(String description, TrafficScotlandData data) {
        this.description = description;
        this.data = data;
    }

    public void parseDates() throws ParseException {

        String dateRegex = "(([0-3])([\\d][\\s][A-z+]{1,10}[\\s+][\\d]{0,4}))";
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
        Pattern pattern = Pattern.compile(dateRegex);
        Matcher matcher = pattern.matcher(description);
        ArrayList<String> dates = new ArrayList<>();
        while (matcher.find()) {
            dates.add(matcher.group());
        }

        if (dates.size() <= 1) {
            // For current incidents since they have no dates, we still need to set the description
            data.setDescription(description);
            data.setStartDate("N/A");
            data.setEndDate("N/A");
        } else {
            data.setStartDate(dates.get(0));
            data.setEndDate(dates.get(1));
            Date startDate = sdf.parse(dates.get(0));
            Date endDate = sdf.parse(dates.get(1));

            Long duration = endDate.getTime() - startDate.getTime();
            Long days = duration / (24 * 60 * 60 * 1000);
            data.setDuration(days);
            data.setDescription(description);
        }
    }

    public void parseDelayInformation() {
        if (description.contains("Delay Information")) {
            String[] delayInformation = description.split("Information: ");
            String information = delayInformation[1];
            data.setShortDescription(information);
        } else if (data.getClass().getSimpleName().equals("CurrentIncidents")) {
            data.setShortDescription(description);
        } else {
            data.setShortDescription("N/A");
        }
    }

    public void parseWorksInformation() {
        if (description.contains("Works")) {
            String worksReg = "(?<=Works:)(.*)(?=Traffic Management)";
            Pattern pattern = Pattern.compile(worksReg);
            Matcher matcher = pattern.matcher(description);
            ArrayList<String> details = new ArrayList<>();
            while (matcher.find()) {
                details.add(matcher.group());
            }
            data.setWorksInformation(details.get(0).toString());
        } else {
            data.setWorksInformation("N/A");
        }
    }

    public void parseTrafficManagementInformation() {
        if (description.contains("Diversion Information")) {
            String trafficManagementReg = "(?<=Traffic Management:)(.*?)(?=Diversion)";
            Pattern pattern = Pattern.compile(trafficManagementReg);
            Matcher matcher = pattern.matcher(description);
            ArrayList<String> details = new ArrayList<>();
            while (matcher.find()) {
                details.add(matcher.group());
            }
            data.setTrafficManagementInformation(details.get(0));
        } else if (description.contains("Diversion Information") == false && description.contains("Traffic Management") == true) {
            String worksReg = "(?<=Traffic Management:)(.*)";
            Pattern pattern = Pattern.compile(worksReg);
            Matcher matcher = pattern.matcher(description);
            ArrayList<String> details = new ArrayList<>();
            while (matcher.find()) {
                details.add(matcher.group());
            }
            if (details.size() > 0) {
                data.setTrafficManagementInformation(details.get(0));
            }
        } else {
            data.setTrafficManagementInformation("N/A");
        }
    }

    public void parseDiversionInformation() {
        if (description.contains("Diversion Information")) {
            String diversionReg = "(?<=Diversion Information:)(.*)";
            Pattern pattern = Pattern.compile(diversionReg);
            Matcher matcher = pattern.matcher(description);
            ArrayList<String> details = new ArrayList<>();
            while (matcher.find()) {
                details.add(matcher.group());
            }
            data.setDiversionDetails(details.get(0));
        } else {
            data.setDiversionDetails("N/A");
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TrafficScotlandData getData() {
        return data;
    }

    public void setData(TrafficScotlandData data) {
        this.data = data;
    }
}
