package mpdproject.gcu.me.org.assignmenttest1.Data;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import mpdproject.gcu.me.org.assignmenttest1.R;

/**
 * Created by ryank on 26/03/2018.
 * Ryan Kerr S1433346
 */

public class HelperDialogSingleton extends AppCompatActivity {

    private static HelperDialogSingleton instance = null;

    protected HelperDialogSingleton() {

    }

    public static HelperDialogSingleton getInstance() {
        if (instance == null) {
            instance = new HelperDialogSingleton();
        }
        return instance;
    }

    public void buildHelpDialog(AppCompatActivity activity) {
        AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        View mView = activity.getLayoutInflater().inflate(R.layout.help_dialog, null);

        Button closeInfo = (Button) mView.findViewById(R.id.closeInfo);
        alert.setView(mView);
        final AlertDialog dialog = alert.create();
        closeInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onRestart();
                dialog.dismiss();
            }
        });

        dialog.show();
    }


}
