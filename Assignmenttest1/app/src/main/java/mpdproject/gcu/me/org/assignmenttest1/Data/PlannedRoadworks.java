package mpdproject.gcu.me.org.assignmenttest1.Data;

/**
 * Created by ryank on 10/02/2018.
 * Ryan Kerr S1433346
 */

public class PlannedRoadworks extends TrafficScotlandData {
    public PlannedRoadworks(){
    }

    @Override
    public String toString() {
        return "PlannedRoadworks{" +
                "title='" + super.getTitle() + '\'' +
                ", description='" + super.getDescription() + '\'' +
                ", link='" + super.getLink() + '\'' +
                ", coordinates=" + super.getCoordinates() +
                ", author='" + super.getAuthor() + '\'' +
                ", comments='" + super.getComments() + '\'' +
                ", date='" + super.getDate() + '\'' +
                '}';
    }
}
