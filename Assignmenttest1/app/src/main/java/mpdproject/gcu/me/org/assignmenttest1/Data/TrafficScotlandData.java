package mpdproject.gcu.me.org.assignmenttest1.Data;

/**
 * Created by ryank on 10/02/2018.
 * Ryan Kerr S1433346
 */

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.ArrayList;

public class TrafficScotlandData implements Serializable, Comparable{
    String title;
    String startDate;
    String endDate;
    String description;
    String link;
    String coordinates;
    String author;
    String comments;
    String date;
    Long duration;
    String shortDescription;
    String worksInformation;
    String trafficManagementInformation;
    String diversionDetails;

    static ArrayList<TrafficScotlandData> dataStore;

    public TrafficScotlandData() {
        this.title = "";
        this.description = "";
        this.link = "";
        this.coordinates = "";
        this.author = "";
        this.comments = "";
        this.date = "";
        this.shortDescription = "";
    }

    public TrafficScotlandData(String title, String description, String link, String author, String comments, String date) {
        this.title = title;
        this.description = description;
        this.link = link;
        this.coordinates = coordinates;
        this.author = author;
        this.comments = comments;
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    protected String getLink() {
        return link;
    }

    public String getCoordinates() {
        return coordinates;
    }

    protected String getAuthor() {
        return author;
    }

    protected String getComments() {
        return comments;
    }

    protected String getDate() {
        return date;
    }

    public ArrayList<TrafficScotlandData> getDataStore(){
        return dataStore;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public String getStartDate() { return startDate; }

    public void setStartDate(String startDate) { this.startDate = startDate; }

    public String getEndDate() {return endDate; }

    public void setEndDate(String endDate) { this.endDate = endDate; }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setDataStore(ArrayList<TrafficScotlandData> data){
        this.dataStore = data;
    }

    public void setShortDescription(String shortDescription){
        this.shortDescription = shortDescription;
    }

    public String getShortDescription(){
        return shortDescription;
    }

    public String getWorksInformation() {
        return worksInformation;
    }

    public void setWorksInformation(String worksInformation) {
        this.worksInformation = worksInformation;
    }

    public String getTrafficManagementInformation() {
        return trafficManagementInformation;
    }

    public void setTrafficManagementInformation(String trafficManagementInformation) {
        this.trafficManagementInformation = trafficManagementInformation;
    }

    public String getDiversionDetails() {
        return diversionDetails;
    }

    public void setDiversionDetails(String diversionDetails) {
        this.diversionDetails = diversionDetails;
    }

    @Override
    public String toString() {
        return "TrafficScotlandData{" +
                "title='" + title + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", description='" + description + '\'' +
                ", link='" + link + '\'' +
                ", coordinates='" + coordinates + '\'' +
                ", author='" + author + '\'' +
                ", comments='" + comments + '\'' +
                ", date='" + date + '\'' +
                ", duration='" + duration + '\'' +
                '}';
    }

    @Override
    public int compareTo(@NonNull Object o) {
        return 0;
    }
}
