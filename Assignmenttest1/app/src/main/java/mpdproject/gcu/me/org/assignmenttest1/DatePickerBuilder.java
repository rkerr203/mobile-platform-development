package mpdproject.gcu.me.org.assignmenttest1;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import java.util.Calendar;

/**
 * Created by ryank on 25/03/2018.
 * Ryan Kerr S1433346
 */

public class DatePickerBuilder extends DialogFragment {
    public Dialog onCreateDialog(Bundle savedInstanceState){
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(), R.style.datepicker, (DatePickerDialog.OnDateSetListener) getActivity(), year, month, day);
    }
}
