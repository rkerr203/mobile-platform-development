package mpdproject.gcu.me.org.assignmenttest1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import mpdproject.gcu.me.org.assignmenttest1.Async.URLTask;
import mpdproject.gcu.me.org.assignmenttest1.Data.HelperDialogSingleton;
import mpdproject.gcu.me.org.assignmenttest1.enums.RSSFeed;

/**
 * Ryan Kerr S1433346
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button currentIncidentsButton;
    private Button plannedRoadworksButton;
    private Button roadworksButton;
    private Button informationButton;

    /**
     * @param savedInstanceState Here we initialise the objects needed to display the main activity, setting the content view and assigning this activity as a listener for the buttons
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        prepareButtons();

        /** To be shown when we load data using the Async URLTask, for now it is hidden */
        findViewById(R.id.loadingPanel).setVisibility(View.GONE);

    }

    /**
     * This method prepares this activity as a listener for their onClick() functionality
     */
    public void prepareButtons() {

        currentIncidentsButton = (Button) findViewById(R.id.incidentsButton);
        currentIncidentsButton.setOnClickListener(this);

        plannedRoadworksButton = (Button) findViewById(R.id.plannedRoadworksButton);
        plannedRoadworksButton.setOnClickListener(this);

        roadworksButton = (Button) findViewById(R.id.roadworksButton);
        roadworksButton.setOnClickListener(this);

        informationButton = (Button) findViewById(R.id.informationButton);
        informationButton.setOnClickListener(this);
    }

    /**
     * This is used to reset the button colors once the Main Activity is restarted
     */
    @Override
    public void onRestart() {

        super.onRestart();
        roadworksButton.setBackgroundResource(R.drawable.button);
        plannedRoadworksButton.setBackgroundResource(R.drawable.button);
        currentIncidentsButton.setBackgroundResource(R.drawable.button);
        informationButton.setBackgroundResource(R.drawable.button);
    }

    /**
     * @param menu
     * @return Look at removing this, or add Home option and help.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    /**
     * @param aview This handles the onClick event of buttons, taking a view as a parameter and matching the ID with the case statements to determine
     *              the correct URL for data retrieval.
     */
    public void onClick(View aview) {
        String url;
        String type;


        switch (aview.getId()) {
            case R.id.incidentsButton:
                url = RSSFeed.CURRENT_INCIDENTS.getUrl();
                type = "INCIDENT";
                aview.setBackgroundResource(R.drawable.buttonpressed);
                new URLTask(this).execute(url, type);
                break;
            case R.id.plannedRoadworksButton:
                url = RSSFeed.PLANNED_ROADWORKS.getUrl();
                type = "ROADWORKS";
                aview.setBackgroundResource(R.drawable.buttonpressed);
                new URLTask(this).execute(url, type);
                break;
            case R.id.roadworksButton:
                url = RSSFeed.ROADWORKS.getUrl();
                type = "CURRENTROADWORKS";
                aview.setBackgroundResource(R.drawable.buttonpressed);
                new URLTask(this).execute(url, type);
                break;
            case R.id.informationButton:
                HelperDialogSingleton dialog = HelperDialogSingleton.getInstance();
                dialog.buildHelpDialog(this);
                break;
            default:
                url = RSSFeed.CURRENT_INCIDENTS.getUrl();
                type = "DEFAULT";
                new URLTask(this).execute(url, type);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.toolbarHelp:
                HelperDialogSingleton dialog = HelperDialogSingleton.getInstance();
                dialog.buildHelpDialog(this);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    /**
     * @param result
     * @param type   This method takes the data returned from the Async URLTask and creates an intent with the data to pass to the TrafficInformationActivity to start it.
     */
    public void startTrafficInformationActivity(final String result, final String type) {

        Intent intent = new Intent(MainActivity.this, TrafficInformationActivity.class);
        intent.putExtra("DATA", String.valueOf(result));
        intent.putExtra("TYPE", type);
        startActivity(intent);
    }

}
