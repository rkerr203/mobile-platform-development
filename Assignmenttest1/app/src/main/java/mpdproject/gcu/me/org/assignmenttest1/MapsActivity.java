package mpdproject.gcu.me.org.assignmenttest1;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import mpdproject.gcu.me.org.assignmenttest1.Data.HelperDialogSingleton;
import mpdproject.gcu.me.org.assignmenttest1.Data.TrafficScotlandData;

/**
 * Ryan Kerr S1433346
 */

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, View.OnClickListener {

    private GoogleMap mMap;

    private Double latitude;
    private Double longitude;
    private String title;
    private String description;
    private TextView startDate;
    private TextView endDate;
    private TextView incidentDescription;
    private TextView titleView;
    private TextView tableTitleView;
    private TextView trafficManagementInfo;
    private TextView diversionDetails;
    private ViewFlipper flipper;
    private TextView infoButton;
    private int markerId;
    private Long durationDetails;
    private String mapSnippet;

    /**
     * Set up the activity, creating toolbar, initialising back button click event, retrieving data we will use to search the selected item against
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            setContentView(R.layout.activity_maps);
        } else {
            setContentView(R.layout.activity_maps_land);
            flipper = (ViewFlipper) findViewById(R.id.flipper);
            flipper.setOnClickListener(this);

            infoButton = (TextView) findViewById(R.id.moreInfo);
            infoButton.setOnClickListener(this);
            TableLayout table = (TableLayout) findViewById(R.id.landscape_table);
            table.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    flipper.showNext();
                }
            });
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TrafficScotlandData data = new TrafficScotlandData();
        ArrayList<TrafficScotlandData> trafficData = data.getDataStore();

        String selectedListViewItem = getIntent().getStringExtra("clickedItem");

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        parseMapInformation(trafficData, selectedListViewItem);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    /**
     * This method finds the selected list item from a list of all data and retrieves the values we need from the TrafficScotlandData object
     * in order to display it in he appropriate text views.
     *
     * @param trafficData
     * @param item
     */
    private void parseMapInformation(ArrayList<TrafficScotlandData> trafficData, String item) {
        for (int i = 0; i < trafficData.size(); i++) {
            if (trafficData.get(i).getTitle().equals(item)) {

                String[] coordinates = trafficData.get(i).getCoordinates().split(" ");
                setLatitude(Double.valueOf(coordinates[0]));
                setLongitude(Double.valueOf(coordinates[1]));


                String delInformation = trafficData.get(i).getShortDescription();

                Long duration = trafficData.get(i).getDuration();
                setMapMarkerIcon(duration);

                TextView workDetails = (TextView) findViewById(R.id.workDetails);
                workDetails.setText(trafficData.get(i).getWorksInformation());

                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    tableTitleView = (TextView) findViewById(R.id.mapTitleTable);
                    tableTitleView.setText(trafficData.get(i).getTitle());
                }

                workDetails.setText(trafficData.get(i).getWorksInformation());

                trafficManagementInfo = (TextView) findViewById(R.id.trafficManagementDetails);
                trafficManagementInfo.setText(trafficData.get(i).getTrafficManagementInformation());

                diversionDetails = (TextView) findViewById(R.id.diversionDetails);
                diversionDetails.setText(trafficData.get(i).getDiversionDetails());


                durationDetails = trafficData.get(i).getDuration();
                if(durationDetails == null){
                    mapSnippet = delInformation;
                }
                else{
                    mapSnippet = "Duration " + durationDetails.toString() + " days";
                }


                startDate = (TextView) findViewById(R.id.startDate);
                endDate = (TextView) findViewById(R.id.endDate);

                startDate.setText(trafficData.get(i).getStartDate());
                endDate.setText(trafficData.get(i).getEndDate());

                title = trafficData.get(i).getTitle();
                titleView = (TextView) findViewById(R.id.mapTitle);
                titleView.setText(title);
                incidentDescription = (TextView) findViewById(R.id.descriptionDetails);
                incidentDescription.setText(delInformation);
            }
        }
    }

    public void setMapMarkerIcon(Long duration) {
        if (duration == null) {
            markerId = R.mipmap.ic_warning;
        } else if (duration >= 0 && duration <= 100) {
            markerId = R.mipmap.road_work_green;
        } else if (duration > 100 && duration <= 500) {
            markerId = R.mipmap.road_work_amber;
        } else {
            markerId = R.mipmap.road_work;
        }
    }

    /**
     * Sets map marker given the parsed values to the incident location
     * Also implements the functionality for showing an info window on marker click showing the given data in the snippet()
     *
     * @param googleMap
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        LatLng position = new LatLng(this.latitude, this.longitude);
        mMap.addMarker(new MarkerOptions().position(position).title(this.title).snippet(mapSnippet).icon(BitmapDescriptorFactory.fromResource(markerId)));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(position));
        mMap.setOnMarkerClickListener(this);

        GoogleMap.InfoWindowAdapter adapter = new GoogleMap.InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker arg0) {

                View v = MapsActivity.this.getLayoutInflater().inflate(R.layout.info_window, null);
                TextView title = (TextView) v.findViewById(R.id.title);
                title.setText(arg0.getTitle());

                TextView date = (TextView) v.findViewById(R.id.startDate);
                date.setText(arg0.getSnippet());
                return v;

            }
        };
        mMap.setInfoWindowAdapter(adapter);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.showInfoWindow();
        return false;
    }

    /**
     * Inflating out options menu for the toolbar, will allow users to view help dialog or go to Main Activity
     * Also attach a listener to the search view, implemented in the SearchListener class
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    /**
     * Handles click events from the options menu, view help or go to Main Activity
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.toolbarHomeOption:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;
            case R.id.toolbarHelp:
                HelperDialogSingleton dialog = HelperDialogSingleton.getInstance();
                dialog.buildHelpDialog(this);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        flipper.showNext();
    }
}
