package mpdproject.gcu.me.org.assignmenttest1;

import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import mpdproject.gcu.me.org.assignmenttest1.Data.TrafficScotlandData;

/**
 * Created by ryank on 25/03/2018.
 */

public class SearchListener implements SearchView.OnQueryTextListener {

    private TrafficInformationActivity activity;
    private ArrayList<TrafficScotlandData> searchData;

    public SearchListener(TrafficInformationActivity activity) {
        this.activity = activity;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {

        ArrayList<String> searchTitles = activity.getDataTitles();
        ArrayList<String> searchResults = new ArrayList<>();

        for (String title : searchTitles) {
            if (title.contains(query)) {
                searchResults.add(title);
            }
        }
        searchData = new ArrayList<>();
        for (String title : searchResults) {
            for (TrafficScotlandData data : activity.getFullData()) {
                if (title.contains(data.getTitle())) {
                    searchData.add(data);
                }
            }
        }

        if (searchResults.size() > 0) {
            activity.setDataImages(searchData);
            activity.setDataTitlesAndDurations(searchData);
            ArrayList<String> listTitles = new ArrayList();
            listTitles.clear();
            listTitles.addAll(searchResults);
            activity.prepareAdapterSearch(listTitles, searchResults);
            activity.getSearchMenuItem().collapseActionView();
            activity.getSearchView().clearFocus();
            activity.findViewById(R.id.backButton).setVisibility(View.VISIBLE);
        }
        else{
            TextView error = (TextView) activity.findViewById(R.id.noData);
            error.setText("No search results found, try a new word or date search or try a new dataset.");
            error.setVisibility(View.VISIBLE);
            activity.findViewById(R.id.backButton).setVisibility(View.VISIBLE);
            activity.findViewById(R.id.filter_spinner).setVisibility(View.GONE);
            activity.getSearchMenuItem().collapseActionView();
            activity.getSearchView().clearFocus();
        }
        return true;
    }

    public void searchByDate(Date date) throws ParseException {
        ArrayList<TrafficScotlandData> dataToSearch = new TrafficScotlandData().getDataStore();
        ArrayList<String> searchResults = new ArrayList<>();
        Date startDate;
        Long time = date.getTime();
        Long time2;
        System.out.println("TIME 1" + time);
        searchData = new ArrayList<>();
        for (TrafficScotlandData data : dataToSearch) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
            startDate = sdf.parse(data.getStartDate());
            time2 = startDate.getTime();
            if(time.toString().equals(time2.toString())){
                searchResults.add(data.getTitle());
                searchData.add(data);
            }
            else{
                System.out.println("NO MATCH + " + time2);
            }

        }

        if (searchResults.size() > 0) {
            System.out.println(searchResults.toString());
            activity.setDataImages(searchData);
            activity.setDataTitlesAndDurations(searchData);
            ArrayList<String> listTitles = new ArrayList();
            listTitles.clear();
            listTitles.addAll(searchResults);
            activity.prepareAdapterSearch(listTitles, searchResults);
            activity.findViewById(R.id.backButton).setVisibility(View.VISIBLE);
        }
        else if(searchResults.size() <= 0){
            activity.findViewById(R.id.noData).setVisibility(View.VISIBLE);
            TextView error = (TextView) activity.findViewById(R.id.noData);
            error.setText("No search results found, try a new word or date search or try a new dataset.");
            error.setVisibility(View.VISIBLE);
        }

    }
    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
}