package mpdproject.gcu.me.org.assignmenttest1;

import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Comparator;

import mpdproject.gcu.me.org.assignmenttest1.Data.TrafficScotlandData;
import mpdproject.gcu.me.org.assignmenttest1.enums.RSSFeed;

/**
 * Created by ryank on 02/03/2018.
 * Ryan Kerr S1433346
 */

public class SpinnerFilter extends Activity implements AdapterView.OnItemSelectedListener {
    private TrafficInformationActivity activity;
    private String currentIncidentsURL = RSSFeed.CURRENT_INCIDENTS.getUrl();
    private String plannedRoadworksURL = RSSFeed.PLANNED_ROADWORKS.getUrl();
    private String roadworksURL = RSSFeed.ROADWORKS.getUrl();
    private String value;

    public SpinnerFilter(TrafficInformationActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        System.out.println(adapterView.getItemAtPosition(i));
        String filter = (String) adapterView.getItemAtPosition(i);
        TrafficScotlandData data = new TrafficScotlandData();
        String type = "";
        String URL = "";

        switch (filter) {
            case "Current Incidents":
                System.out.println("Incidents");
                type = "INCIDENT";
                URL = currentIncidentsURL;
                break;
            case "Roadworks":
                System.out.println("Roadworks");
                type = "CURRENTROADWORKS";
                URL = roadworksURL;
                break;
            case "Planned Roadworks":
                System.out.println("Planned Roadworks");
                type = "ROADWORKS";
                URL = plannedRoadworksURL;
                break;
            default:
                System.out.println("DEFAULT");
        }

        if (value.equals(filter)) {
            System.out.println("Already selected");
            value = "";
        } else {
            System.out.println("Spinner selection TYPE " + type);
            activity.setActivityTitle(type);
            activity.changeData(URL, type);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public int getIndex(Spinner spinner, String type) {
        System.out.println("SPINNER " + type);
        int index = 0;
        value = "";
        switch (type) {
            case "INCIDENT":
                value = "Current Incidents";
                break;
            case "ROADWORKS":
                value = "Planned Roadworks";
                break;
            case "CURRENTROADWORKS":
                value = "Roadworks";
                break;
            default:
                value = "Roadworks";
                break;
        }
        System.out.println("Value " + value);
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).equals(value)) {
                index = i;
                System.out.println("SPINNER FILER FOR " + spinner.getItemAtPosition(i));
            }
        }
        activity.setTitle(type);
        return index;
    }


    public void prepareSpinner(String type){
            Spinner spinner = (Spinner) activity.findViewById(R.id.filter_spinner);
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(activity,
                    R.array.spinner_array, R.layout.single_spinner_item);
            adapter.setDropDownViewResource(R.layout.spinner_dropdown);
            spinner.setAdapter(adapter);
            spinner.setSelection(this.getIndex(spinner, type));
            spinner.setOnItemSelectedListener(this);

    }
}
