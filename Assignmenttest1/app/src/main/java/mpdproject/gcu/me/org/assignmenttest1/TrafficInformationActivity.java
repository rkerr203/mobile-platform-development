package mpdproject.gcu.me.org.assignmenttest1;
import android.app.DatePickerDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import mpdproject.gcu.me.org.assignmenttest1.Async.URLTask;
import mpdproject.gcu.me.org.assignmenttest1.Data.CustomListAdapter;
import mpdproject.gcu.me.org.assignmenttest1.Data.HelperDialogSingleton;
import mpdproject.gcu.me.org.assignmenttest1.Data.TrafficScotlandData;
import mpdproject.gcu.me.org.assignmenttest1.XMLParser.XMLPullParser;

/**
 * Created by ryank on 10/02/2018.
 * Ryan Kerr S1433346
 */


public class TrafficInformationActivity extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private String data;
    private ListView list;
    private String type;
    private SearchView searchView;
    private MenuItem searchMenuItem;
    private ArrayAdapter<String> listAdapter;
    private Button allResults;
    private Integer[] durationIcons;
    private String[] titleArray;
    private TrafficScotlandData dataStore;
    private ArrayList<String> titles = new ArrayList<>();
    private ArrayList<Long> durations = new ArrayList<>();
    private ArrayList<TrafficScotlandData> fullData;
    private TextView noData;

    public TrafficInformationActivity() {
    }

    /**
     * Creating everything we need for the Traffic activity, this one activity displays data for each dataset by updating the list contents.
     * Some setup is explicit in the method and the rest done via method calls to keep the onCreate as uncluttered as possible.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.incidents_activity);

        data = getIntent().getStringExtra("DATA");
        type = getIntent().getStringExtra("TYPE");
        dataStore = new TrafficScotlandData();
        noData = (TextView) findViewById(R.id.noData);
        noData.setVisibility(View.GONE);
        prepareData(data, type);

        Toolbar toolbar = (Toolbar) findViewById(R.id.search_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getSupportActionBar().setTitle("");

        setActivityTitle(type);

        prepareSearchBackButton();

        findViewById(R.id.trafficLoadingPanel).setVisibility(View.GONE);
    }

    /**
     * Inflating out options menu for the toolbar, will allow users to view help dialog or go to Main Activity
     * Also attach a listener to the search view, implemented in the SearchListener class
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if(type.equals("INCIDENT")){
            getMenuInflater().inflate(R.menu.incidents_options_menu, menu);
        }
        else{
            getMenuInflater().inflate(R.menu.traffic_options_menu, menu);
        }
        searchMenuItem = menu.findItem(R.id.search);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) searchMenuItem.getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchListener(this));

        return true;
    }

    /**
     * To allow users to go back to all results without going back to the Main Activity, this button is shown.
     * On restart it should be hidden.
     */
    @Override
    public void onRestart() {
        super.onRestart();
        if(getSearchMenuItem() != null){
            getSearchMenuItem().collapseActionView();
            getSearchView().clearFocus();
        }
        allResults.setVisibility(View.GONE);
    }

    /**
     * Sets up the listener for the back button
     */
    public void prepareSearchBackButton() {
        allResults = (Button) findViewById(R.id.backButton);
        allResults.setOnClickListener(this);
        allResults.setVisibility(View.GONE);
    }

    /**
     * This delegates the Spinner creation to the SpinnerFilter class
     * This class handles creation and inflation of options
     *
     * @param type
     */
    public void prepareSpinner(String type) {
        SpinnerFilter filter = new SpinnerFilter(this);
        filter.prepareSpinner(type);
    }

    /**
     * Returns a menu item, used within the SearchListener class
     *
     * @return
     */
    public MenuItem getSearchMenuItem() {
        return searchMenuItem;
    }


    /**
     * Returns the search view from onCreateOptionsMenu(), used in SearchListener class
     *
     * @return
     */
    public SearchView getSearchView() {
        return searchView;
    }

    /**
     * This prepares the adapter when data is updated during a search
     *
     * @param titles
     * @param results
     */
    public void prepareAdapterSearch(ArrayList<String> titles, ArrayList<String> results) {
        findViewById(R.id.noData).setVisibility(View.GONE);
        list = (ListView) findViewById(R.id.simpleListView);
        listAdapter = new CustomListAdapter(this, titles, durationIcons, durations);
        list.setAdapter(listAdapter);
        listAdapter.clear();
        listAdapter.addAll(results);
        list.setOnItemClickListener((AdapterView.OnItemClickListener) listAdapter);
        listAdapter.notifyDataSetChanged();
    }

    /**
     * Handles click events from the options menu, view help or go to Main Activity
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.toolbarHomeOption:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;
            case R.id.toolbarHelp:
                HelperDialogSingleton dialog = HelperDialogSingleton.getInstance();
                dialog.buildHelpDialog(this);
                break;
            case R.id.datePicker:
                DialogFragment datePicker = new DatePickerBuilder();

                datePicker.show(getSupportFragmentManager(), "Search by date");

                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    /**
     * Delegates data parsing to the XMLPull Parser and initialises variables and Customer list adapter for data output
     * Uses helper methods to complete all the tasks, setDataTitles(), and later setDataImages()
     *
     * @param data
     * @param type
     */
    public void prepareData(String data, String type) {
            prepareSpinner(type);
            list = (ListView) findViewById(R.id.simpleListView);

            XMLPullParser parser = new XMLPullParser();
            dataStore.setDataStore(parser.parseData(data, type));

            // Pass data to method that will populate list view
            if(dataStore.getDataStore().size() == 0){
                noData.setText("No data currently available, select Home from the options menu or go back to choose another dataset");
                noData.setVisibility(View.VISIBLE);
            }
            else {
                noData.setVisibility(View.GONE);
                setDataTitlesAndDurations(dataStore.getDataStore());
                ArrayList<String> listTitles = new ArrayList<>(Arrays.asList(titleArray));
                listAdapter = new CustomListAdapter(this, listTitles, durationIcons, durations);
                list.setOnItemClickListener((AdapterView.OnItemClickListener) listAdapter);
                list.setAdapter(listAdapter);
            }
        }

    /**
     * Updates the titles used in the listView
     *
     * @param test
     */
    public void setDataTitlesAndDurations(ArrayList<TrafficScotlandData> test) {
        titles.clear();
        durations.clear();
        this.durationIcons = new Integer[test.size()];
        this.fullData = test;
        for (int i = 0; i < test.size(); i++) {
            titles.add(test.get(i).getTitle());
            durations.add(test.get(i).getDuration());
        }

        titleArray = new String[test.size()];
        for (int i = 0; i < test.size(); i++) {
            titleArray[i] = test.get(i).getTitle();
        }

        setDataImages(test);
    }

    /**
     * A helper method for prepareData() that sets the appropriate image for each listView item
     *
     * @param test
     * @return
     */
    public Integer[] setDataImages(ArrayList<TrafficScotlandData> test) {
        // Recreating array solves index = length issues
        this.durationIcons = new Integer[test.size()];
        for (int i = 0; i < test.size(); i++) {
            if (test.get(i).getClass().getSimpleName().equals("CurrentIncidents")) {
                durationIcons[i] = R.mipmap.ic_warning;
            } else {
                if (test.get(i).getDuration() <= 100) {
                    durationIcons[i] = R.mipmap.road_work_green;
                } else if (test.get(i).getDuration() > 100 && test.get(i).getDuration() < 500) {
                    durationIcons[i] = R.mipmap.road_work_amber;
                } else if (test.get(i).getDuration() > 500) {
                    durationIcons[i] = R.mipmap.road_work;
                }
            }
        }
        return durationIcons;
    }

    /**
     * Whats this for>*******************************************?
     *
     * @param aview
     */
    public void onClick(View aview) {

        // Recreate the adapter here to ensure the images and title data match
        setDataTitlesAndDurations(dataStore.getDataStore());
        listAdapter.addAll(titles);
        allResults.setVisibility(View.GONE);

        ArrayList<String> listTitles = new ArrayList<>(titles);
        listAdapter = new CustomListAdapter(this, listTitles, this.durationIcons, durations);
        list.setOnItemClickListener((AdapterView.OnItemClickListener) listAdapter);
        list.setAdapter(listAdapter);

        if(getSearchMenuItem() != null){
            getSearchMenuItem().collapseActionView();
            getSearchView().clearFocus();
        }

    }

    public void updateTitles(ArrayList<String> titles) {

        listAdapter.clear();
        listAdapter.addAll(titles);
        listAdapter.notifyDataSetChanged();
    }

    /**
     * Used to display data received from the Async URLTask
     *
     * @param result
     * @param type
     */
    public void displayData(final String result, final String type) {

        prepareData(result, type);
    }

    /**
     * Sets the title in the Toolbar to reflect the dataset chosen
     *
     * @param type
     */
    public void setActivityTitle(String type) {
        switch (type) {
            case "INCIDENT":
                getSupportActionBar().setTitle("Incidents");
                break;
            case "ROADWORKS":
                getSupportActionBar().setTitle("Planned Roadworks");
                break;
            case "CURRENTROADWORKS":
                getSupportActionBar().setTitle("Roadworks");
                break;
        }
    }

    /**
     * Gets data when spinner item is clicked and displays it
     *
     * @param url
     * @param task
     */
    public void changeData(String url, String task) {
        new URLTask(this).execute(url, task);
    }

    public ArrayList getDataTitles() {
        return this.titles;
    }

    public ArrayAdapter<String> getListAdapter() {
        return this.listAdapter;
    }

    public ArrayList<TrafficScotlandData> getFullData() {
        return fullData;
    }

    public void setFullData(ArrayList<TrafficScotlandData> fullData) {
        this.fullData = fullData;
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND,0);

        Date currentDate = calendar.getTime();

        SearchListener listener = new SearchListener(this);
        try {
            listener.searchByDate(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
}

