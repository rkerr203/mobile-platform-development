package mpdproject.gcu.me.org.assignmenttest1.XMLParser;

/**
 * Created by ryank on 10/02/2018.
 * Ryan Kerr S1433346
 */


import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.util.ArrayList;

import mpdproject.gcu.me.org.assignmenttest1.Data.CurrentIncidents;
import mpdproject.gcu.me.org.assignmenttest1.Data.DescriptionParser;
import mpdproject.gcu.me.org.assignmenttest1.Data.PlannedRoadworks;
import mpdproject.gcu.me.org.assignmenttest1.Data.Roadworks;
import mpdproject.gcu.me.org.assignmenttest1.Data.TrafficScotlandData;

public class XMLPullParser {

    private ArrayList<TrafficScotlandData> dataObjects = new ArrayList<>();

    public XMLPullParser() {
    }

    public ArrayList parseData(String data, String type) {
        TrafficScotlandData dataStore = null;
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();
            xpp.setInput(new StringReader(data));
            int eventType = xpp.getEventType();

            for (int i = 0; i < 10; i++) {

                if (eventType == XmlPullParser.START_DOCUMENT) {
                    try {
                        xpp.next();
                        System.out.println("Skipping to first item ... ");
                    } catch (XmlPullParserException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    if (xpp.getName().equalsIgnoreCase("item")) {
                        switch (type) {
                            case "INCIDENT":
                                dataStore = new CurrentIncidents();
                                break;
                            case "CURRENTROADWORKS":
                                dataStore = new Roadworks();
                                break;
                            case "ROADWORKS":
                                dataStore = new PlannedRoadworks();
                                break;
                            default:
                                dataStore = new TrafficScotlandData();
                        }
                    } else if (xpp.getName().equalsIgnoreCase("title")) {

                        if (dataStore != null) {
                            dataStore.setTitle(xpp.nextText());
                        }
                    } else if (xpp.getName().equalsIgnoreCase("description")) {
                        if (dataStore != null) {
                            String description = xpp.nextText();
                            DescriptionParser parser = new DescriptionParser(description, dataStore);
                            parser.parseDates();
                            parser.parseDelayInformation();
                            parser.parseWorksInformation();
                            parser.parseTrafficManagementInformation();
                            parser.parseDiversionInformation();
                            dataStore = parser.getData();

                        }
                    } else if (xpp.getName().equalsIgnoreCase("link")) {
                        if (dataStore != null) {
                            dataStore.setLink(xpp.nextText());
                        }
                    } else if (xpp.getName().equalsIgnoreCase("point")) {
                        System.out.println("GEO " + xpp.getName());
                        if (dataStore != null) {
                            System.out.println("GEO " + xpp.getName());
                            dataStore.setCoordinates(xpp.nextText());
                        }
                    } else
                        // Check which Tag we have
                        if (xpp.getName().equalsIgnoreCase("author")) {
                            if (dataStore != null) {
                                dataStore.setAuthor(xpp.nextText());
                            }
                        } else
                            // Check which Tag we have
                            if (xpp.getName().equalsIgnoreCase("comments")) {
                                if (dataStore != null) {
                                    dataStore.setComments(xpp.nextText());
                                }
                            } else
                                // Check which Tag we have
                                if (xpp.getName().equalsIgnoreCase("pubDate")) {
                                    if (dataStore != null) {
                                        dataStore.setDate(xpp.nextText());
                                    }
                                }
                } else if (eventType == XmlPullParser.END_TAG) {
                    if (xpp.getName().equalsIgnoreCase("item")) {
                        dataObjects.add(dataStore);
                    } else if (xpp.getName().equalsIgnoreCase("title")) {
                        System.out.println(xpp.getName());
                    } else if (xpp.getName().equalsIgnoreCase("link")) {
                        System.out.println(xpp.getName());
                    }
                }

                eventType = xpp.next();

            }
        } catch (XmlPullParserException ae1) {
            Log.e("MyTag", "Parsing error" + ae1.toString());
        } catch (IOException ae1) {
            Log.e("MyTag", "IO error during parsing");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        System.out.println("End document");
        return dataObjects;
    }

}
