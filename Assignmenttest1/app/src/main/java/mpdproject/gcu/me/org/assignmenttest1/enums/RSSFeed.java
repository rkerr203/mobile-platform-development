package mpdproject.gcu.me.org.assignmenttest1.enums;

import java.security.PrivateKey;

/**
 * Created by ryank on 10/02/2018.
 * Ryan Kerr S1433346
 */

public enum RSSFeed {

    CURRENT_INCIDENTS("http://trafficscotland.org/rss/feeds/currentincidents.aspx"),
    PLANNED_ROADWORKS("http://trafficscotland.org/rss/feeds/plannedroadworks.aspx"),
    ROADWORKS("http://trafficscotland.org/rss/feeds/roadworks.aspx");

    private final String url;

    RSSFeed(String s) {
        this.url = s;
    }

    public String getUrl() {
        return url;
    }
}
